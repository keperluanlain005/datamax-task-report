# Report
Laporan pengerjaan 2 buah task yaitu
1. Installasi dan config Nginx, Nodejs dan NPM untuk deploy aplikasi rest api nodejs, menggunakan nginx sebagai reserve proxy dengan domian test1.datamax.id disertai SSL certificate
2. Installasi dan config docker, docker-compose dan docker swarm untuk mendeploy aplikasi nodejs yang di containerize dan dicompose dengan container nginx yang diconfig agar terinstall ssl certificate

# Pengerjaan Task 1

## Langkah pengerjaan

1. Install NPM dengan metode nodesource dan Nodejs

    ![Install Nodejs dan NPM](images/1.png)
    ![Install Nodejs dan NPM](images/2.png)
2. Install nginx

    ![Install Nginx](images/3.png)
3. Install git

    ![Install GIT](images/4.png)
4. Add user mimin, pull, npm install dan mencoba menjalankan aplikasi

    ![Pull and RUN](images/7.png)
    `Aplikasi dipull pada directory`
    ```
    /home/mimin/app/simple-api/
    ```
5. Membuat service pada systemd untuk run dan auto start 
aplikasi saat boot
    ![Systemd Config](images/6.png)

    `Script yang digunakan`
    ```yaml
    [Unit]
    After=network.target

    [Service]
    ExecStart=/usr/bin/node /home/mimin/app/simple-api/server.js
    Type=simple
    Restart=always
    StandardOutput=syslog
    TimeoutSec=90
    SyslogIdentifier=simpe-api
    User=mimin
    Environment=PATH=/usr/bin:/usr/local/bin
    Environment=NODE_PORT=4000

    [Install]
    WantedBy=multi-user.target
    ```
6. Test simpe-api.service

    ![Test Service](images/8.png)
    
    `Akses melalui browser`
    
    ![Akses api](images/18.png)
7. Membuat Self SSL Certificate menggunakan  [Localca](https://github.com/amitn322/localca)

    Langkah yang perlu diperhatikan adalah
    * Setelah pull dari repository, berikan permission execute pada file `createCA.sh` dan `CreateCert.sh`
    * jalankan `createCA.sh` hingga selesai dan muncul output datamax_cert dengan format `[crt, pem , srl]`, setelah itu langsung jalankan `CreateCert.sh` dan nanti akan memasukan datamax_cert sebagai parameter
    * langkah berikutnya copy `test1.datamax.id.[crt, key]` ke dalam `/etc/ssl/`
    ![Create SSL Certificate](images/9.png)
8. Configurasi Nginx

    Membuat file `test1.datamax.id` pada directory `/etc/nginx/conf.d` yang berisikan config sebagai berikut
    ```nginx
    server {
        listen 443 http2 ssl;
        listen [::]:443 http2 ssl;

        server_name test1.datamax.id;

        ssl_certificate /etc/ssl/certs/test1.datamax.id.crt;
        ssl_certificate_key /etc/ssl/private/test1.datamax.id.key;

        ########################################################################
        # from https://cipherli.st/                                            #
        # and https://raymii.org/s/tutorials/Strong_SSL_Security_On_nginx.html #
        ########################################################################

        ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
        ##################################
        # END https://cipherli.st/ BLOCK #
        ##################################

        root /usr/share/nginx/html;

        location / {
        proxy_pass http://localhost:4000;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection 'upgrade';
        proxy_set_header Host $host;
        proxy_cache_bypass $http_upgrade;
        }

        error_page 404 /404.html;
        location = /404.html {
        }

        error_page 500 502 503 504 /50x.html;
        location = /50x.html {
        }
    }
    ```
    pada baris dibawah ini, kita menggunakan sertifikat yang sudah dipasang pada directory /etc/ssl
    ```
    ssl_certificate /etc/ssl/certs/test1.datamax.id.crt;
    ssl_certificate_key /etc/ssl/private/test1.datamax.id.key;
    ```
    setelah selesai melakukan configurasi pada nginx, test nginx config dengan sintak
    ```
    sudo nginx -t
    ```
    jika tidak ada error restart nginx dengan sintak
    ```
    sudo systemctl restart nginx
    ```
9. Akses aplikasi dengan alamat `https://test1.datamax.id`
    ![Akses Aplikasi](images/10.png)

# Pengerjaan Task 2

## Langkah pengerjaan

1. Installasi docker dengan metode _convenience script_
    ```
    curl -fsSL https://get.docker.com -o get-docker.sh
    sudo sh get-docker.sh
    ```
    langkah berikutnya adalah :
    * memasukan user mimin ke dalam group docker agar `rootless`
    * menginstall `docker-compose`
    * membuat folder `dockerize` pada dicretory `~/` lalu copy aplikasi pada directory `~/app/` 
![Dockerize](images/11.png)
2. Persiapan Dockerize

    ![Langkah 1](images/12.png)
    * Membuat folder `node` di dalam `simple-api` lalu memindahkan semua file pada directory `simple-api` kedalamnya
    * membuat folder `nginx` sebagai wadah configurasi image nginx
    * membuat file `default` pada directory `nginx` sebagai configurasi default, sebagai berikut :
        ```nginx
        server {
            listen 80 default_server;

            listen 443 ssl;

            root /config/www;
            index index.html;

            server_name test1.datamax.id;

            ssl_certificate /config/keys/cert.crt;
            ssl_certificate_key /config/keys/cert.key;

            client_max_body_size 0;

            location / {
                proxy_pass http://simple-api:4000;
                proxy_http_version 1.1;
                proxy_set_header Upgrade $http_upgrade;
                proxy_set_header Connection 'upgrade';
                proxy_set_header Host $host;
                proxy_cache_bypass $http_upgrade;
            }
        }
        ```
    * membuat `Dockerfile` pada directory `nginx` sebagai berikut : 
        ```Dockerfile
        FROM ghcr.io/linuxserver/nginx:latest

        RUN rm -f /config/nginx/site-confs/default

        COPY default /config/nginx/site-confs/
        COPY test1.datamax.id.crt /config/keys/cert.crt
        COPY test1.datamax.id.key /config/keys/cert.key
        ```
        saya menggunakan image `nginx` dari [Linux Server](linuxserver.io) karena sudah di config dengan baik sehingga memudahkan deploy `ssl`
    * Copy ssl file dari directory `~/ssl/` ke dalam directory `nginx`
    * Membuat `docker-compose.yml` pada direcory `simple-api` untuk compose `node` dan `nginx` dengan configurasi sebagai berikut :
        ```yaml
        version: '3.3'

        services:
        simple-api:
            image: 127.0.0.1:5000/simple-api
            build:
            context: ./node
            deploy:
            replicas: 2
            volumes:
            - rest-api-vol:/usr/src/app
            restart: unless-stopped
            networks:
            - app-network

        nginx:
            image: 127.0.0.1:5000/nginx
            build:
            context: ./nginx
            environment:
            - PUID=1000
            - PGID=1000
            - TZ=Asia/Jakarta
            ports:
            - "80:80"
            - "443:443"
            restart: unless-stopped
            depends_on:
            - simple-api
            networks:
            - app-network

        volumes:
        rest-api-vol:
            external: true

        networks:
        app-network:
        ```
    * Langkah berikutnya adalah membuat docker `volume` dengan nama `rest-api-vol` dengan perintah
        ```
        docker volume create rest-api-vol
        ```
    * Mengaktifkan `Docker swarm`
    ![Docker Swarm](images/13.png)
3. Membuat local `registry` untuk menyimpan image hasil `docker-compose`
![DOcker Registry](images/14.png)
4. Compose down lalu push image ke local registry
![Push Image](images/15.png)
5. Menjalankan docker-compose.yml kedalam swarm
![Docker Swarm Compose](images/17.png)
    * Untuk menjalankan compose file pada docker swarm, gunakan sintak berikut:
    ``` bash
    docker stack deploy --compose-file docker-compose.yml simple-api

    ```
    * untuk melihat service yang berjalan pada docker swarm stack :
    ```
    docker stack services simple-api
    ```
    * melihat docker volume : 
    ```
    docker volume inspect --format '{{ .Mountpoint }}' rest-api-vol
    ```
6. SSL terpasang dengan baik saat di akses dari browser dengan alamat `https://test1.datamax.id`
![Akses Dari Browser](images/10.png)
